# pull the official docker image
FROM python:3.10.2-slim
# set work directory
WORKDIR /api

RUN apt-get update \
    && apt-get -y install libpq-dev gcc

# install dependencies
COPY requirements.txt .
RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

# copy project
COPY . .

# Run the application
CMD ["sh", "-c", "uvicorn app.main:app --host 0.0.0.0 --workers 5"]