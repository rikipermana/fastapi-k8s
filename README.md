# FastApi Kubernetes

Kubernetes config on `k8s` folder

CI/CD using `.gitlab-ci.yml`

## Public IP

Cluster status : **off**

### Development

http://139.59.192.75/

#### API Documentation

http://139.59.192.75/docs

### Staging

http://137.184.248.69/

#### API Documentation

http://137.184.248.69/docs

## API Setup

### RUN Migration

`alembic upgrade head`

### Local Setup

`python -m virtualenv venv`

powershell: `./venv/Scripts/activate.ps1`

`pip install -r requirements.txt`

### How to RUN

`uvicorn app.main:app --host localhost --port 8000 --reload`
